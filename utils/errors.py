class DFSError(Exception):
    pass


class FileDoesntExist(DFSError):
    def __init__(self, path, *args, **kwargs):
        Exception.__init__(self, f"File at {path} doesnt exist", args, kwargs)


class FileAlreadyExists(DFSError):
    def __init__(self, path, *args, **kwargs):
        Exception.__init__(self, f"File at {path} already exists", args, kwargs)


class DirectoryAlreadyExists(DFSError):
    def __init__(self, path, *args, **kwargs):
        Exception.__init__(self, f"Directory at {path} already exists", args, kwargs)


class DeleteRootError(DFSError):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, "Asked to delete the root of the filesystem", args, kwargs)


class DirectoryNotAFile(DFSError):
    def __init__(self, path, *args, **kwargs):
        Exception.__init__(self, f"Expected to find a directory at {path}, but got file", args, kwargs)


class FileNotADirectory(DFSError):
    def __init__(self, path, *args, **kwargs):
        Exception.__init__(self, f"Expected to find a file at {path}, but got directory", args, kwargs)


class StorageIsDown(DFSError):
    def __init__(self, task_name):
        Exception.__init__(self, f"Unable to process {task_name} because all storage nodes are down")


class DFSNotInited(DFSError):
    def __init__(self):
        Exception.__init__(self, "Unable to process request, need to init the DFS first")

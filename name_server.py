import logging
import os
from concurrent import futures

import grpc

from generated import routes_pb2_grpc, routes_pb2
from servers import NameService
from utils import FileDoesntExist, DFSError, StorageIsDown


class RouteGuideServicer(routes_pb2_grpc.NameServiceServicer):
    """Provides methods that implement functionality of route guide server."""

    @staticmethod
    def get_path_and_name(path):
        file_path = os.sep.join(path.split(os.sep)[:-1])
        filename = path.split(os.sep)[-1]
        if not file_path:
            file_path = '/'
        return file_path, filename

    def Init(self, request, context):
        path = f"{os.environ.get('NAME_SERVER_DATA_PATH', './name_data')}"
        size = name_service.init_filesystem(path)
        return routes_pb2.InitInfo(available_size=size)

    def GetFileInfo(self, request, context):
        try:
            info = name_service.info_file(request.file_path)
        except FileDoesntExist as e:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details(str(e))
            return routes_pb2.FileInfo()
        except DFSError as e:
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details(str(e))
            return routes_pb2.FileInfo()
        return routes_pb2.FileInfo(name=info.name, size=info.size,
                                   created_date=str(info.created_time),
                                   file_uuid=info.uuid_name)

    def GetFile(self, request, context):
        file_path, filename = self.get_path_and_name(request.file_path)
        try:
            file_uuid = name_service.read_file(file_path, filename)
        except FileDoesntExist as e:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details(str(e))
            return routes_pb2.FileWithServersResponse()
        except DFSError as e:
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details(str(e))
            return routes_pb2.FileWithServersResponse()
        return routes_pb2.FileWithServersResponse(file_uuid=file_uuid, storages=[routes_pb2.Storage(url=i) for i in
                                                                                 name_service.get_server_urls()])

    def MoveFile(self, request, context):
        source_file_path, source_filename = self.get_path_and_name(request.file_path)
        target_file_path, target_filename = self.get_path_and_name(request.new_path)
        try:
            file_uuid = name_service.move_file(source_file_path, source_filename, target_file_path, target_filename)
        except FileDoesntExist as e:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details(str(e))
            return routes_pb2.FileResponse()
        except DFSError as e:
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details(str(e))
            return routes_pb2.FileResponse()
        return routes_pb2.FileResponse(file_uuid=file_uuid)

    def DeleteFile(self, request, context):
        file_path, filename = self.get_path_and_name(request.file_path)
        try:
            uuid = name_service.delete_file(file_path, filename)
        except FileDoesntExist as e:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details(str(e))
            return routes_pb2.FileResponse()
        except DFSError as e:
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details(str(e))
            return routes_pb2.FileResponse()
        return routes_pb2.FileResponse(file_uuid=uuid)

    def StoreFile(self, request, context):
        file_path, filename = self.get_path_and_name(request.file_path)
        try:
            file_uuid = name_service.create_file(file_path, filename, request.size)
        except FileDoesntExist as e:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details(str(e))
            return routes_pb2.FileWithServersResponse()
        except DFSError as e:
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details(str(e))
            return routes_pb2.FileWithServersResponse()
        return routes_pb2.FileWithServersResponse(file_uuid=file_uuid, storages=[routes_pb2.Storage(url=i) for i in
                                                                                 name_service.get_server_urls()])

    def CopyFile(self, request, context):
        source_file_path, source_filename = self.get_path_and_name(request.file_path)
        target_file_path, target_filename = self.get_path_and_name(request.new_path)
        try:
            file_uuid_source, file_uuid_target = name_service.copy_file(source_file_path, source_filename,
                                                                        target_file_path,
                                                                        target_filename)
        except FileDoesntExist as e:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details(str(e))
            return routes_pb2.CopyFileResponse()
        except StorageIsDown as e:
            context.set_code(grpc.StatusCode.INTERNAL)
            context.set_details(str(e))
            return routes_pb2.CopyFileResponse()
        except DFSError as e:
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details(str(e))
            return routes_pb2.CopyFileResponse()
        return routes_pb2.CopyFileResponse(source_file_uuid=file_uuid_source, target_file_uuid=file_uuid_target)

    def GetDirectoryInfo(self, request, context):
        try:
            files = name_service.read_directory(request.path)
        except FileDoesntExist as e:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details(str(e))
            return routes_pb2.Directory()
        except DFSError as e:
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details(str(e))
            return routes_pb2.Directory()

        dir_name = request.path.split(os.sep)[-1]
        if not dir_name:  # if it's empty then use root
            dir_name = '/'
        fil = []
        for file in files[dir_name]:
            fil.append(routes_pb2.DirectoryFileInfo(name=file.name, size=file.size, is_file=file.is_file(),
                                                    created_date=str(file.created_time)))
        return routes_pb2.Directory(name=dir_name, path=request.path, files=fil)

    def DeleteDirectory(self, request, context):
        try:
            dir = name_service.delete_directory(request.path)
        except FileDoesntExist as e:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details(str(e))
            return routes_pb2.DirectoryInfo()
        except DFSError as e:
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details(str(e))
            return routes_pb2.DirectoryInfo()
        return routes_pb2.DirectoryInfo(name=dir)

    def StoreDirectory(self, request, context):
        try:
            dir = name_service.make_directory(request.path)
        except FileDoesntExist as e:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details(str(e))
            return routes_pb2.DirectoryInfo()
        except DFSError as e:
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details(str(e))
            return routes_pb2.DirectoryInfo()

        return routes_pb2.DirectoryInfo(name=dir)

    def Heartbeat(self, request, context):
        response = name_service.update_storage(request.url, request.available_size)
        servers = [routes_pb2.Storage(url=url) for url in response['servers']]
        files = [routes_pb2.FileResponse(file_uuid=file_uuid) for file_uuid in response['files']]
        return routes_pb2.HeartbeatResponse(init=name_service.was_inited(), storages=servers,
                                            files=files)

    def FileAcknowledge(self, request, context):
        name_service.ack_files(request.file_uuid)
        return routes_pb2.AckResponse()


def serve(server_port):
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    routes_pb2_grpc.add_NameServiceServicer_to_server(RouteGuideServicer(), server)
    server.add_insecure_port(f"[::]:{server_port}")
    server.start()
    server.wait_for_termination()


if __name__ == "__main__":
    logging.basicConfig()

    if os.environ.get('SENTRY_DSN'):
        import sentry_sdk
        sentry_sdk.init(os.environ.get('SENTRY_DSN'))

    server_port = int(os.environ.get("NAME_SERVER_PORT", 8000))
    print(f"Name server started on port {server_port}")
    name_service = NameService()
    serve(server_port)

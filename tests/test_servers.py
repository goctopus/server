import json
import os
import shutil

import pytest

from server.servers import Directory, File, NameService, StorageService
import uuid

from utils import FileDoesntExist


class TestFilesDirectories:
    def setup(self):
        self.root = Directory("/")
        self.data = Directory("data", parent=self.root)
        self.goc = Directory("goctopus", parent=self.data)
        self.another_file = File("dat", self.goc, str(uuid.uuid1()), metainfo={"size": 1}, acked=True)
        self.file2 = File("sel", self.data, str(uuid.uuid1()), metainfo={"size": 1}, acked=True)
        self.file = File("lol", self.root, str(uuid.uuid1()), metainfo={"size": 1}, acked=True)

        self.root.children.append(self.file)
        self.root.children.append(self.data)
        self.data.children.append(self.file2)
        self.data.children.append(self.goc)
        self.goc.children.append(self.another_file)

    def test_creation(self):
        self.setup()
        assert self.root.parent is None
        assert len(self.root.children)
        assert self.root.name == "/"
        assert not self.root.is_file()

        assert self.file.name == "lol"
        assert self.file.parent == self.root
        assert self.file.metainfo["size"] == 1
        assert self.file.is_file()
        self.root.children.append(self.file)

    def test_traverse(self):
        self.setup()
        assert self.another_file.traverse([]) == self.another_file
        with pytest.raises(FileDoesntExist):
            self.another_file.traverse(["smth"])
        with pytest.raises(FileDoesntExist):
            self.root.traverse(["smth"])
        with pytest.raises(FileDoesntExist):
            self.root.traverse(["data", "goctopus", "smth"])
        assert self.goc == self.root.traverse(["data", "goctopus"])
        assert self.file2 == self.root.traverse(["data", "sel"])
        assert self.another_file == self.root.traverse(["data", "goctopus", "dat"])
        assert self.file == self.root.traverse(["lol"])

    def test_to_json(self):
        self.setup()
        dic1 = self.another_file.to_json()
        assert dic1["name"] == self.another_file.name
        assert dic1["uuid_name"] == self.another_file.uuid_name
        assert dic1["metainfo"] == self.another_file.metainfo
        assert dic1["file"] == self.another_file.is_file()
        self.another_file.acked = True
        self.file.acked = True
        self.file2.acked = True
        dic2 = self.root.to_json()
        assert dic2["name"] == self.root.name
        assert dic2["file"] == self.root.is_file()
        assert len(dic2["children"]) == len(self.root.children)
        assert sum([self.file.name in l.values() for l in dic2["children"]])
        assert sum([self.data.name in l.values() for l in dic2["children"]])
        assert not sum([self.file2.name in l.values() for l in dic2["children"]])

    def test_remove_child(self):
        self.setup()
        with pytest.raises(Exception):
            self.root.remove_child(self.file2.name)
        l = len(self.root.children)
        self.root.remove_child(self.data.name)
        assert l - 1 == len(self.root.children)

    def test_find_child(self):
        self.setup()
        assert self.root.find_child(self.data.name) is not None
        assert self.root.find_child(self.file.name) is not None
        assert self.root.find_child(self.file2.name) is None

    def test_restore(self):
        self.setup()
        js = json.dumps(self.root.to_json())
        js2 = json.loads(js)
        directory = Directory.restore_tree(js2)
        assert self.root.name == directory.name
        assert len(self.root.children) == len(directory.children)

        assert self.data.name == directory.children[directory.find_child(self.data.name)].name
        assert self.file.name == directory.children[directory.find_child(self.file.name)].name
        assert self.file.uuid_name == directory.children[directory.find_child(self.file.name)].uuid_name
        assert self.file.metainfo == directory.children[directory.find_child(self.file.name)].metainfo


class TestNameServer:
    def setup(self):
        self.path = "./data"
        self.name = NameService()

    def test_creation(self):
        self.setup()
        self.name.init_filesystem(self.path)
        assert self.name._tree is not None
        assert self.name._path == self.path
        self.name._clear_fs()
        assert self.name._path is None
        assert self.name._tree is None

    def test_parse_path(self):
        list = self.name._parse_path("/hello/there/mlady.ru")
        assert len(list) == 3
        assert list[0] == "hello"
        assert list[1] == "there"
        assert list[2] == "mlady.ru"
        list = self.name._parse_path("/")
        assert len(list) == 0
        list = self.name._parse_path("/hello")
        assert len(list) == 1
        assert list[0] == "hello"

    def test_mkdir(self):
        self.setup()
        self.name.init_filesystem(self.path)
        self.name.make_directory("/data")
        assert len(self.name._tree.children) == 1
        assert self.name._tree.find_child("data") is not None
        with pytest.raises(Exception):
            self.name.make_directory("/data")
        self.name.make_directory("/go/go")
        assert len(self.name._tree.children) == 2
        assert self.name._tree.find_child("go") is not None
        go = self.name._tree.find_child("go")
        go = self.name._tree.children[go]
        assert go.name == "go"
        assert not go.is_file()
        assert go.find_child("go") is not None
        go2 = go.find_child("go")
        go2 = go.children[go2]
        assert go2.name == "go"
        assert not go2.is_file()

    def test_create_file(self):
        self.setup()
        self.name.init_filesystem(self.path)
        self.name.make_directory("/data")
        with pytest.raises(Exception):
            self.name.create_file("/data/data", "go.txt", 1)
        assert len(self.name._tree.children) == 1
        self.name.create_file("/", "go.txt", 1)
        assert len(self.name._tree.children) == 2
        file = self.name._tree.find_child("go.txt")
        file = self.name._tree.children[file]
        assert file.uuid_name is not None
        assert file.metainfo is not None
        assert file.metainfo["size"] == 1
        assert file.is_file()
        self.name.create_file("/data", "go.txt", 1)
        data = self.name._tree.find_child("data")
        data = self.name._tree.children[data]
        assert len(data.children) == 1
        assert data.children[0].name == "go.txt"
        assert data.children[0].is_file()
        with pytest.raises(Exception):
            self.name.create_file("/", "go.txt", 1)

    def test_rmdir(self):
        self.setup()
        self.name.init_filesystem(self.path)
        self.name.make_directory("/data")
        self.name.make_directory("/go")
        assert len(self.name._tree.children) == 2
        with pytest.raises(Exception):
            self.name.delete_directory("/")
        self.name.delete_directory("/data")
        assert len(self.name._tree.children) == 1
        self.name.create_file("/", "go.txt", 1)
        assert len(self.name._tree.children) == 2
        with pytest.raises(Exception):
            self.name.delete_directory("/go.txt")
        assert len(self.name._tree.children) == 2

    def test_info_file(self):
        self.setup()
        self.name.init_filesystem(self.path)
        self.name.make_directory("/data")
        self.name.create_file("/", "go.txt", 1)
        self.name.create_file("/data", "go.txt", 1)
        assert self.name.info_file("/go.txt").is_file()
        assert not self.name.info_file("/").is_file()
        assert not self.name.info_file("/data").is_file()
        assert self.name.info_file("/data/go.txt").is_file()
        with pytest.raises(Exception):
            self.name.info_file("/kek.txt")

    def test_read_dir(self):
        self.setup()
        self.name.init_filesystem(self.path)
        self.name.make_directory("/data")
        self.name.make_directory("/go")
        assert len(self.name._tree.children) == 2
        data = self.name.read_directory("/")
        assert "/" in data
        assert len(data["/"]) == 2
        assert data["/"][0][2] == {}
        assert not data["/"][0][1]
        with pytest.raises(Exception):
            self.name.read_directory("/kek")

    def test_read_file(self):
        self.setup()
        self.name.init_filesystem(self.path)
        self.name.make_directory("/data")
        self.name.create_file("/", "go.txt", 1)
        self.name.create_file("/data", "go.txt", 1)
        file = self.name._tree.find_child("go.txt")
        file = self.name._tree.children[file]
        assert self.name.read_file("/", "go.txt") == file.uuid_name
        data = self.name._tree.find_child("data")
        data = self.name._tree.children[data]
        file = data.find_child("go.txt")
        file = data.children[file]
        assert self.name.read_file("/data", "go.txt") == file.uuid_name
        with pytest.raises(Exception):
            self.name.read_file("/", 'xd.txt')

    def test_rm_file(self):
        self.setup()
        self.name.init_filesystem(self.path)
        self.name.make_directory("/data")
        self.name.create_file("/", "go.txt", 1)
        self.name.create_file("/data", "go.txt", 1)
        file = self.name._tree.find_child("go.txt")
        file = self.name._tree.children[file]
        assert len(self.name._tree.children) == 2
        assert self.name.delete_file("/", file.name) == file.uuid_name
        assert len(self.name._tree.children) == 1
        data = self.name._tree.find_child("data")
        data = self.name._tree.children[data]
        file = data.find_child("go.txt")
        file = data.children[file]
        assert len(data.children) == 1
        assert self.name.delete_file("/data", file.name) == file.uuid_name
        assert len(data.children) == 0
        with pytest.raises(Exception):
            self.name.delete_file("/", "kek.txt")

    def test_mv_file(self):
        self.setup()
        self.name.init_filesystem(self.path)
        self.name.make_directory("/data")
        self.name.create_file("/", "go.txt", 1)
        with pytest.raises(Exception):
            self.name.move_file("/", "go2.txt", "/data", "go2.txt")
        self.name.create_file("/data", "go.txt", 1)
        with pytest.raises(Exception):
            self.name.move_file("/", "go.txt", "/data", "go.txt")
        with pytest.raises(Exception):
            self.name.move_file("/", "go.txt", "/kek", "go.txt")
        assert len(self.name._tree.children) == 2
        file1 = self.name._tree.find_child("go.txt")
        file1 = self.name._tree.children[file1]
        data = self.name._tree.find_child("data")
        data = self.name._tree.children[data]
        assert len(data.children) == 1
        self.name.move_file("/", "go.txt", "/data", "go2.txt")
        file = data.find_child("go2.txt")
        file = data.children[file]
        assert len(self.name._tree.children) == 1
        assert len(data.children) == 2
        assert file.uuid_name == file1.uuid_name

    def test_cp_file(self):
        self.setup()
        self.name.init_filesystem(self.path)
        self.name.make_directory("/data")
        self.name.create_file("/", "go.txt", 1)
        with pytest.raises(Exception):
            self.name.copy_file("/", "go2.txt", "/data", "go2.txt")
        self.name.create_file("/data", "go.txt", 1)
        with pytest.raises(Exception):
            self.name.copy_file("/", "go.txt", "/data", "go.txt")
        with pytest.raises(Exception):
            self.name.copy_file("/", "go.txt", "/kek", "go.txt")
        assert len(self.name._tree.children) == 2
        file1 = self.name._tree.find_child("go.txt")
        file1 = self.name._tree.children[file1]
        data = self.name._tree.find_child("data")
        data = self.name._tree.children[data]
        assert len(data.children) == 1
        returned = self.name.copy_file("/", "go.txt", "/data", "go2.txt")
        file = data.find_child("go2.txt")
        file = data.children[file]
        assert len(self.name._tree.children) == 2
        assert len(data.children) == 2
        assert file.uuid_name != file1.uuid_name
        assert file.uuid_name == returned[1]
        assert file1.uuid_name == returned[0]
        assert file.name == "go2.txt"

    def test_dump_restore(self):
        self.setup()
        self.name.init_filesystem(self.path)
        self.name.make_directory("/data")
        self.name.create_file("/", "go.txt", 1)
        self.name.create_file("/data", "go.txt", 1)
        self.name._dump_tree()
        assert os.path.exists(os.path.join(self.name._path, self.name._tree_file))
        tree = self.name._tree
        self.name._tree = None
        self.name._restore_tree()
        assert self.name._tree.name == tree.name
        file1 = tree.find_child("go.txt")
        file1 = tree.children[file1]
        assert self.name.info_file("/go.txt").uuid_name == file1.uuid_name
        self.name._clear_fs()
        assert not os.path.exists(self.path)


class TestStorageServer:
    def setup(self):
        self.path = "./data"
        self.storage = StorageService("localhost")
        self.storage.init_filesystem(self.path)

    def teardown(self):
        shutil.rmtree(self.path)

    def test_create_file(self):
        id = str(uuid.uuid1())
        file_bytes = b'123'
        self.storage.create_file(id, file_bytes)
        filepath = self.storage.get_path_by_uuid(id)
        print(filepath)
        assert os.path.exists(filepath)
        file = open(filepath, "rb")
        data = file.read()
        file.close()
        assert data == file_bytes

    def test_read_file(self):
        id = str(uuid.uuid1())
        file_bytes = b'123'
        self.storage.create_file(id, file_bytes)
        assert file_bytes == self.storage.read_file(id)

    def test_rm_file(self):
        id = str(uuid.uuid1())
        file_bytes = b'123'
        self.storage.create_file(id, file_bytes)
        filepath = self.storage.get_path_by_uuid(id)
        assert os.path.exists(filepath)
        self.storage.delete_file(id)
        assert not os.path.exists(filepath)

    def test_cp_file(self):
        id1 = str(uuid.uuid1())
        file_bytes = b'123'
        self.storage.create_file(id1, file_bytes)
        id2 = str(uuid.uuid1(1))
        self.storage.copy_file(id1, id2)
        filepath = self.storage.get_path_by_uuid(id2)
        assert os.path.exists(filepath)
        file = open(filepath, "rb")
        data = file.read()
        file.close()
        assert data == file_bytes

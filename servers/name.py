import datetime
import logging
import os
import pickle
import uuid
import datetime
from typing import List, Dict, Union
import json
import shutil

import grpc

from generated import routes_pb2_grpc, routes_pb2
from utils import FileDoesntExist, DirectoryAlreadyExists, DirectoryNotAFile, FileAlreadyExists, DeleteRootError, \
    StorageIsDown, DFSNotInited, FileNotADirectory


class File:
    def __init__(self, name: str, parent: 'Directory', size=0, uuid_name: str = None, metainfo: dict or 'None' = None,
                 acked=False, created_time=None):
        if metainfo is None:
            metainfo = {}
        self.parent: Directory = parent
        self.name: str = name
        self.metainfo: dict = metainfo
        self.uuid_name: str = uuid_name
        self.acked = acked
        self.size = size
        self.created_time = created_time
        if self.created_time is None:
            self.created_time = datetime.datetime.now()

    def __str__(self):
        return self.name

    def traverse(self, path: List[str]) -> 'File':
        if len(path) == 0:
            return self
        else:
            raise FileDoesntExist(path)

    def is_file(self) -> bool:
        return True


class Directory(File):
    def __init__(self, name: str, parent: 'Directory' = None):
        super(Directory, self).__init__(name, parent, acked=True)
        self.children: List[Directory or File] = []

    def is_file(self) -> bool:
        return False

    def traverse(self, path: List[str]) -> 'Directory' or 'File':
        if len(path) >= 1:
            child_idx = self.find_child(path[0])
            if child_idx is not None:
                child = self.children[child_idx]
                return child.traverse(path[1:])
            else:
                raise FileDoesntExist(path)
        else:
            return super(Directory, self).traverse(path)

    def remove_child(self, child):
        child_idx = self.find_child(child)
        if child_idx is not None:
            self.children.pop(self.find_child(child))
        else:
            raise IndexError(f"No children found in directory with index {child}")

    def find_child(self, child) -> int or 'None':
        for i in range(len(self.children)):
            if self.children[i].name == child:
                return i
        return None

    def get_files(self, all=False):
        file_list = []
        for child in self.children:
            if child.is_file():
                if child.acked or all:
                    file_list += [child]
                elif datetime.datetime.now() - child.created_time > datetime.timedelta(minutes=5):
                    logger.debug(f"Acknowledge for file {child.name} expired {child.created_time}, removing from DFS")
                    self.remove_child(child.name)
            else:
                file_list += child.get_files(all)
        return file_list


logger = logging.getLogger("NameService")
logger.setLevel(os.environ.get('LOG_LEVEL', 'DEBUG'))


class Storage:
    def __init__(self, url, available_size):
        self.url = url
        self.available_size = available_size
        self.last_contacted = datetime.datetime.now()

    def __eq__(self, other):
        if type(other) == str:
            return other == self.url
        elif type(other) == Storage:
            return other.url == self.url
        else:
            return False

    def is_dead(self):
        if datetime.datetime.now() - self.last_contacted > datetime.timedelta(seconds=15):
            return True
        return False

    def update_storage(self, available_size):
        self.available_size = available_size
        self.last_contacted = datetime.datetime.now()


class NameService:

    def __init__(self, hostname: str = "localhost", port: int = 8989):
        logger.info("Created Instance of Name Service")
        self._tree_file = "tree.json"
        self.hostname: str = hostname
        self.port: int = port
        self.dfs_size = 0
        self._path: str or 'None' = os.environ.get('NAME_SERVER_DATA_PATH', './name_data')
        try:
            self._restore_tree()
        except FileNotFoundError:
            self._tree: Directory or 'None' = None
            self._files_list: List[File] = []
        self._storage_nodes: List[Storage] = []

    def _restore_tree(self):
        logger.debug("Restored Tree")
        fs = open(os.path.join(self._path, self._tree_file), "rb")
        self._tree = pickle.load(fs)
        self._files_list = self._tree.get_files()
        fs.close()

    def _dump_tree(self):
        logger.debug("Dumped tree")
        fs = open(os.path.join(self._path, self._tree_file), "wb")
        pickle.dump(self._tree, fs)
        fs.close()

    def was_inited(self):
        return self._tree is not None

    def ack_files(self, file_uuid):
        logger.debug(f"Got ack for file {file_uuid}")
        for files in self._tree.get_files(True):
            if files.uuid_name == file_uuid:
                files.acked = True
                break
        self._files_list = self._tree.get_files()
        self._dump_tree()

    def init_filesystem(self, path):
        logger.info(f"Begin initialization of new filesystem at {path}")
        if self.was_inited():
            # There already exists a filesystem, clear previous one init a new one
            logger.info(f"Removing already existent dfs")
            self._clear_fs()
        self._path = path
        self._tree = Directory(os.sep)
        if os.path.exists(self._path):
            logger.info(f"Clearing old files in {path}")
            shutil.rmtree(self._path)
        os.mkdir(self._path)
        s = self._init_storage_nodes()
        logger.debug("Done initializing")
        return s

    def _init_storage_nodes(self):
        logger.info(f"Initializing {len(self._storage_nodes)} storage servers")
        s = []
        for storage in self._storage_nodes:
            try:
                with grpc.insecure_channel(storage.url) as storage_channel:
                    storage_stub = routes_pb2_grpc.StorageServiceStub(storage_channel)
                    request = routes_pb2.InitRequest()
                    req = storage_stub.Init(request)
                    s.append(req.available_size)
                    logger.debug(f"Server {storage.url} was inited")
            except grpc.RpcError as e:
                if e.code() == grpc.StatusCode.UNAVAILABLE:
                    logger.debug(f"Storage {storage.url} is down, skip initing")
                else:
                    logger.debug(e)
                # If not is dead - we just don't init it
        if not s:
            s = [0]
        self.dfs_size = min(s)
        return self.dfs_size

    def _clear_fs(self):
        if os.path.exists(self._path):
            shutil.rmtree(self._path)
        self._path = None
        self._tree = None
        self._files_list = []

    def _parse_path(self, path) -> List[str]:
        # Return file or dir by the path
        assert path[0] == "/"
        split = os.path.normpath(path).split(os.path.sep)
        split.pop(0)
        if not split[-1]:
            split.pop(-1)
        return split

    def update_storage(self, url, available_size):
        logger.info(f"Got heartbeat from {url}, updating info")
        idx = None
        for i in range(len(self._storage_nodes)):
            if self._storage_nodes[i] == url:
                idx = i
        if idx is None:
            self.add_storage(Storage(url, available_size))
        else:
            self._storage_nodes[idx].update_storage(available_size)
        return self.get_upd_info(url)

    def get_server_urls(self):
        return [storage.url for storage in self._storage_nodes if not storage.is_dead()]

    def get_upd_info(self, url):
        # In this case at least one storage node need to be alive
        logger.debug(f"Preparing update info for {url}")
        servers = self.get_server_urls()
        servers.remove(url)
        files = [file.uuid_name for file in self._files_list]
        return {"servers": servers, "files": files}

    def add_storage(self, storage):
        logger.info("New storage was added")
        self._storage_nodes.append(storage)

    def info_file(self, path) -> 'File' or 'Directory':
        if not self.was_inited():
            logger.debug("Tried to perform a file info without init")
            raise DFSNotInited
        logger.debug(f"Getting input from {path}")
        # Allow for info the directory
        return self._tree.traverse(self._parse_path(path))

    def read_directory(self, path) -> dict:
        if not self.was_inited():
            logger.debug("Tried to perform a directory reading without init")
            raise DFSNotInited
        logger.debug(f"Reading directory from {path}")
        directory = self._tree.traverse(self._parse_path(path))
        if directory.is_file():
            logger.debug(f"Expected to find a directory at {path} got file")
            raise DirectoryNotAFile(path)
        return {
            directory.name: [child for child in
                             directory.children if
                             child.acked]}

    def make_directory(self, path):
        if not self.was_inited():
            logger.debug("Tried to perform a directory cretion without init")
            raise DFSNotInited
        logger.debug(f"Making new directory in {path}")
        dirs = self._parse_path(path)
        cur_dir = self._tree
        for i in range(len(dirs)):
            child_idx = cur_dir.find_child(dirs[i])
            if child_idx is None:
                break
            cur_dir = cur_dir.children[child_idx]
        if child_idx is not None:
            logger.error(f"Directory at path {path} already exists")
            raise DirectoryAlreadyExists(path)
        else:
            # Create dirs
            for j in range(i, len(dirs)):
                directory = Directory(dirs[j], parent=cur_dir)
                cur_dir.children.append(directory)
                cur_dir = directory
            self._dump_tree()
            return cur_dir.name

    def delete_directory(self, path):
        if not self.was_inited():
            logger.debug("Tried to perform a directory deletion without init")
            raise DFSNotInited
        logger.debug(f"Removing directory at {path}")
        directory = self._tree.traverse(self._parse_path(path))
        if directory.is_file():
            logger.error(f"Removing a directory, but found a file")
            raise DirectoryNotAFile(path)
        parent = directory.parent
        if parent is None:
            logger.error(f"Asked to delete the root of the dfs")
            raise DeleteRootError
        parent.remove_child(directory.name)
        self._dump_tree()
        return directory.name

    def create_file(self, path, filename, size):
        if not self.was_inited():
            logger.debug("Tried to perform a file creation without init")
            raise DFSNotInited
        logger.debug(f"Creating file {filename} with size {size} at {path}")
        directory: Directory = self._tree.traverse(self._parse_path(path))
        if directory.find_child(filename) is not None:
            logger.error(f"File {path + '/' + filename} already exists")
            raise FileAlreadyExists(path + filename)
        else:
            uuid_name = str(uuid.uuid1())
            file = File(filename, uuid_name=uuid_name, parent=directory, size=size)
            directory.children.append(file)
            self._files_list = self._tree.get_files()
            self._dump_tree()
            return uuid_name

    def read_file(self, path, filename) -> str:
        if not self.was_inited():
            logger.debug("Tried to perform a file reading without init")
            raise DFSNotInited
        logger.debug(f"Reading file {filename} at {path}")
        directory: Directory = self._tree.traverse(self._parse_path(path))
        child_idx: int = directory.find_child(filename)
        if child_idx is None:
            logger.error(f"File {path + '/' + filename} doesn't exist")
            raise FileDoesntExist(path + filename)
        else:
            if not directory.children[child_idx].is_file():
                logger.error(f"Expected to read a file, got a directory at {path + filename}")
                raise FileNotADirectory(path + filename)
            return directory.children[child_idx].uuid_name

    def delete_file(self, path, filename) -> str:
        if not self.was_inited():
            logger.debug("Tried to perform a file deletion without init")
            raise DFSNotInited
        logger.debug(f"Deleting file {filename} at {path}")
        directory: Directory = self._tree.traverse(self._parse_path(path))
        child_idx: int = directory.find_child(filename)
        if child_idx is None:
            logger.error(f"File {path + '/' + filename} doesn't exist")
            raise FileDoesntExist(path + filename)
        else:
            if not directory.children[child_idx].is_file():
                logger.error(f"Expected to delete a file, got a directory at {path + filename}")
                raise FileNotADirectory(path + filename)
            child = directory.children.pop(child_idx)
            for storage in self._storage_nodes:
                try:
                    with grpc.insecure_channel(storage.url) as storage_channel:
                        storage_stub = routes_pb2_grpc.StorageServiceStub(storage_channel)
                        request = routes_pb2.StorageFileInfo(file_uuid=child.uuid_name)
                        storage_stub.RemoveFile(request)
                except grpc.RpcError as e:
                    if e.code() == grpc.StatusCode.UNAVAILABLE:
                        logger.debug(f"Storage node {storage.url} is down, skipping delete")
                    elif e.code() == grpc.StatusCode.NOT_FOUND:
                        logger.debug(f"Storage node {storage.url} don't have the file {child.uuid_name}")
                    else:
                        logger.debug(e)
            self._files_list = self._tree.get_files()
            self._dump_tree()
            return child.uuid_name

    def move_file(self, path_source, filename, path_target, new_filename):
        # Works with directories
        if not self.was_inited():
            logger.debug("Tried to perform a file move without init")
            raise DFSNotInited
        logger.debug(f"Moving file {path_source + filename} to {path_target + new_filename}")
        directory_source: Directory = self._tree.traverse(self._parse_path(path_source))
        child_idx: int = directory_source.find_child(filename)
        if child_idx is None:
            logger.error(f"File {path_source + filename} doesn't exist")
            raise FileDoesntExist(path_source + filename)
        else:
            child = directory_source.children[child_idx]

            directory_target: Directory = self._tree.traverse(self._parse_path(path_target))
            if directory_target.find_child(new_filename) is not None:
                logger.error(f"File {path_target + new_filename} already exists")
                raise FileAlreadyExists(path_target + new_filename)

            child.parent = directory_target
            child.name = new_filename
            directory_source.remove_child(filename)
            directory_target.children.append(child)
            self._dump_tree()
            return child.uuid_name

    def copy_file(self, path_source, filename, path_target, new_filename) -> (str, str):
        if not self.was_inited():
            logger.debug("Tried to perform a file copy without init")
            raise DFSNotInited
        logger.debug(f"Copying file {path_source + filename} to {path_target + new_filename}")
        directory_source: Directory = self._tree.traverse(self._parse_path(path_source))
        child_idx: int = directory_source.find_child(filename)

        if child_idx is None:
            logger.error(f"File {path_source + filename} doesn't exist")
            raise FileDoesntExist(path_source + filename)
        else:
            child = directory_source.children[child_idx]
            if not child.is_file():
                logger.error(f"Expected to copy a file, got a directory at {path_source + filename}")
                raise FileNotADirectory(path_source + filename)

            directory_target: Directory = self._tree.traverse(self._parse_path(path_target))
            if directory_target.find_child(new_filename) is not None:
                logger.error(f"File {path_target + new_filename} already exists")
                raise FileAlreadyExists(path_target + new_filename)

            uuid_name = str(uuid.uuid1())
            file = File(new_filename, uuid_name=uuid_name, parent=directory_target, size=child.size, acked=True)
            directory_target.children.append(file)
            count = 0
            for storage in self._storage_nodes:
                try:
                    with grpc.insecure_channel(storage.url) as storage_channel:
                        storage_stub = routes_pb2_grpc.StorageServiceStub(storage_channel)
                        request = routes_pb2.CopyFileRequest(source_file_uuid=child.uuid_name,
                                                             target_file_uuid=uuid_name)
                        storage_stub.CopyFile(request)
                        count += 1
                except grpc.RpcError as e:
                    if e.code() == grpc.StatusCode.UNAVAILABLE:
                        logger.debug(f"Failed to proceed with copy, server {storage.url} is down")
                    elif e.code() == grpc.StatusCode.NOT_FOUND:
                        logger.debug(f"Storage {storage.url} don't have file {child.uuid_name}")
                    elif e.code() == grpc.StatusCode.INVALID_ARGUMENT:
                        logger.debug(f"Storage {storage.url} already have file {uuid_name}")
                    else:
                        logger.debug(e)
                    # Don't need to do anything after that as the files will be replicated by others
            if count == 0:
                logger.debug("All storage servers are down")
                raise StorageIsDown(f"CopyFile({path_source + filename},{path_target + new_filename})")
            self._files_list = self._tree.get_files()
            self._dump_tree()
            return child.uuid_name, uuid_name

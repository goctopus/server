import datetime
import logging
import shutil

import os
from threading import Thread
from time import sleep
from typing import List

import grpc

from generated import routes_pb2_grpc, routes_pb2
from utils import FileDoesntExist, FileAlreadyExists, DFSNotInited

logger = logging.getLogger("StorageService")
logger.setLevel(os.environ.get('LOG_LEVEL', 'DEBUG'))


class StorageService:

    def __init__(self, url: str):
        logger.info("Created Instance of Storage Service")
        self.url: str = url
        self.name_hostname: str = os.environ.get('NAME_SERVER_URL')
        self.name_port: int = int(os.environ.get("NAME_SERVER_PORT"))
        self._path: str or 'None' = os.environ.get('STORAGE_SERVER_DATA_PATH')
        self._daemon_thread: 'None' = None
        self._storages: List[str] = []
        self._files: List[str] = []
        if self.was_inited():
            self.restore_files()

    def restore_files(self):
        for r, d, f in os.walk(self._path):
            for file in f:
                self._files.append(os.path.join(r[len(self._path) + 1:], file).replace("/", "-"))

    def heartbeat_daemon(self):
        logger.info("Initiating heartbeat thread")
        self._daemon_thread = Thread(target=self._start_daemon, daemon=True)
        self._daemon_thread.start()

    def ack_file(self, file_uuid):
        logger.debug(f"Acknowledging file {file_uuid}")
        done = False
        while not done:
            try:
                with grpc.insecure_channel(f'{self.name_hostname}:{self.name_port}') as name_channel:
                    name_stub = routes_pb2_grpc.NameServiceStub(name_channel)
                    req = routes_pb2.AckRequest(file_uuid=file_uuid)
                    res = name_stub.FileAcknowledge(req)
                    done = True
            except grpc.RpcError as e:
                done = False
                sleep(10)
                logger.debug(f"Failed to acknowledge {file_uuid} server {self.name_hostname}:{self.name_port} is down")

    def _start_daemon(self):
        retry = 15
        while True:
            logger.debug(f"Trying to contact the server at {self.name_hostname}")
            name_url = f'{self.name_hostname}:{self.name_port}'
            with grpc.insecure_channel(name_url) as name_channel:
                name_stub = routes_pb2_grpc.NameServiceStub(name_channel)
                if self.was_inited():
                    total, used, free = shutil.disk_usage(self._path)
                else:
                    free = 0
                try:
                    heartbeat = routes_pb2.HeartbeatRequest(url=self.url, available_size=free)
                    req = name_stub.Heartbeat(heartbeat)
                    if req.init and not self.was_inited():
                        self.init_filesystem(os.environ.get('STORAGE_SERVER_DATA_PATH'))
                    self.update_fs(req.storages, req.files)
                    logger.debug("Got Response from the server, continue working")
                    retry = 15
                except grpc.RpcError as e:
                    if e.code() == grpc.StatusCode.UNAVAILABLE:
                        retry = 5
                        logger.info(f"Namenode at {self.name_hostname} not available, retrying in {retry} seconds")
            sleep(retry)

    def get_modify_time(self, file_uuid):
        path = self.get_path_by_uuid(file_uuid)
        return datetime.datetime.fromtimestamp(os.path.getmtime(path))

    def update_fs(self, servers, files):
        logger.info("Got update from nameserver, proceeding")
        name_files = set([file.file_uuid for file in files])
        to_load = name_files.difference(self._files)
        self._storages = [server.url for server in servers]
        for file in set(self._files).difference(name_files):
            if datetime.datetime.now() - self.get_modify_time(file) > datetime.timedelta(minutes=5):
                logger.debug(f"Found dangling file {file}")
                self.delete_file(file)
        loaded = set()
        for server in servers:
            for file in to_load:
                if to_load == loaded:
                    break
                logger.debug(f"Found not loaded file {file}")
                try:
                    with grpc.insecure_channel(server.url) as storage_channel:
                        storage_stub = routes_pb2_grpc.StorageServiceStub(storage_channel)
                        request = routes_pb2.GetFileStorageRequest(file_uuid=file)
                        req = storage_stub.GetFile(request)
                        self.create_file(file, req.file, propagate=False)
                        logger.debug(f"Got file {file} from {server.url}")
                        loaded.add(file)
                except grpc.RpcError as e:
                    if e.code() == grpc.StatusCode.UNAVAILABLE:
                        logger.debug(f"Storage {server.url} is unavailable for download, trying another")
                    elif e.code() == grpc.StatusCode.NOT_FOUND:
                        logger.debug(f"Storage don't have a file {file}, skipping")
                    else:
                        logger.debug(e)
                except FileAlreadyExists:
                    loaded.add(file)  # File already created, skip it

    def was_inited(self):
        return os.path.exists(self._path)

    def init_filesystem(self, path):
        logger.info(f"Begin initialization of new filesystem at {path}")
        if self.was_inited():
            # There already exists a filesystem, clear previous one init a new one
            logger.info(f"Removing already existent dfs")
            self._clear_fs()
        self._path = path
        if os.path.exists(self._path):
            logger.info(f"Clearing old files in {path}")
            shutil.rmtree(self._path)
        os.makedirs(self._path)
        total, used, free = shutil.disk_usage(self._path)
        logger.debug("Done initializing")
        return free

    def file_exists(self, file_uuid: str):
        return file_uuid in self._files

    def _clear_fs(self):
        if os.path.exists(self._path):
            shutil.rmtree(self._path)
        self._path = None
        self._files = []

    def get_path_by_uuid(self, file_uuid: str):
        path = os.path.join(self._path, file_uuid.replace("-", os.sep))
        return path

    def create_file(self, file_uuid: str, file_bytes: bytes, propagate=True):
        if not self.was_inited():
            logger.debug("Tried to perform a file creation without init")
            raise DFSNotInited
        path = self.get_path_by_uuid(file_uuid)
        if self.file_exists(file_uuid):
            raise FileAlreadyExists(path)
        dirs = path.split(os.sep)[:-1]
        os.makedirs(os.sep.join(dirs))
        file = open(path, "wb")
        file.write(file_bytes)
        file.close()
        self.ack_file(file_uuid)
        self._files.append(file_uuid)
        logger.debug(f"Creating file {file_uuid}")
        if propagate or propagate is None:
            if self._storages:
                logger.info(f"Replicating to {len(self._storages)} storages")
            for storage in self._storages:
                try:
                    with grpc.insecure_channel(storage) as storage_channel:
                        storage_stub = routes_pb2_grpc.StorageServiceStub(storage_channel)
                        request = routes_pb2.StorageFile(file_uuid=file_uuid, size=len(file_bytes), file=file_bytes,
                                                         propagate=False)
                        storage_stub.StoreFile(request)
                except grpc.RpcError as e:
                    if e.code() == grpc.StatusCode.UNAVAILABLE:
                        logger.debug(f"Storage {storage} is unavailable for creating a file, skipping")
                    elif e.code() == grpc.StatusCode.INVALID_ARGUMENT:
                        logger.debug(f"Storage {storage} already has the file {file_uuid}")
                    else:
                        logger.debug(e)
                    # Skip if node isnt available

    def read_file(self, file_uuid: str):
        if not self.was_inited():
            logger.debug("Tried to perform a file reading without init")
            raise DFSNotInited
        path = self.get_path_by_uuid(file_uuid)
        if not os.path.exists(path):
            raise FileDoesntExist(path)
        file = open(path, "rb")
        data = file.read()
        file.close()
        logger.debug(f"Reading file {file_uuid}")
        return data

    def delete_file(self, file_uuid: str):
        if not self.was_inited():
            logger.debug("Tried to perform a file deletion without init")
            raise DFSNotInited
        path = self.get_path_by_uuid(file_uuid)
        if not os.path.exists(path):
            raise FileDoesntExist(path)
        os.remove(path)
        self._files.remove(file_uuid)
        logger.debug(f"Deleting file {file_uuid}")

    def copy_file(self, file1_uuid: str, file2_uuid: str):
        if not self.was_inited():
            logger.debug("Tried to perform a file copying without init")
            raise DFSNotInited
        source = self.get_path_by_uuid(file1_uuid)
        target = self.get_path_by_uuid(file2_uuid)
        if not os.path.exists(source):
            raise FileDoesntExist(source)
        if os.path.exists(target):
            raise FileAlreadyExists(target)
        split = target.split(os.sep)
        os.makedirs(target[:-len(split[-1])])
        shutil.copyfile(source, target)
        self._files.append(file2_uuid)
        logger.debug(f"Copying file {file1_uuid} to {file2_uuid}")

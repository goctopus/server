FROM python:3.7
WORKDIR /app
ENV PYTHONUNBUFFERED 1
COPY requirements.txt .
RUN pip3 install --cache-dir=/cache -r requirements.txt
COPY . .

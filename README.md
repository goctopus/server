# README
# Project Structure
Repositories:

* [**Common**](https://gitlab.com/goctopus/common) - protobuf declaration
* [**Frontend**]() - web client
* [**Servers**](https://gitlab.com/goctopus/frontend) - backend part, DFS itself

# Gitlab Container Registry with images
* [Name node and Storage node Images](https://gitlab.com/goctopus/server/container_registry) - `registry.gitlab.com/goctopus/server/develop` with tag from the last commit hash
* [Web Client Image](https://gitlab.com/goctopus/frontend/container_registry) - `registry.gitlab.com/goctopus/frontend/develop` with tag from last commit hash


# How to launch

Launch DFS:
1. Install Docker (version 19.03.5 or newer) on at least 4 nodes and join them into Swarm.
2. Pull images from Gitlab Registry on each node:
`docker login registry.gitlab.com`
`docker-compose -f docker-compose.swarm.yml pull`
1. Start DFS from manager node:
`docker stack deploy -c docker-compose.swarm.yml dfs_prod`
1. Stop DFS:
`docker stack rm dfs_prod`

Launch DFS client:
1. Install Docker (version 19.03.5 or newer) on at least 1 node and join them into Swarm (if there are more than 1 nodes)
2. Pull images from Gitlab Registry on each node:
`docker login registry.gitlab.com`
`docker-compose -f docker-compose.yml pull`
1. Start DFS client from manager node:
`docker stack deploy -c docker-compose.yml dfs_frontprod`
1. Stop DFS client:
`docker stack rm dfs_frontprod`

# Contribution
* Vladislav Savchuk - backend part of DFS (Name node and Storage nodes)
* Timur Mustafin - a bit of everything, Docker Swarm, CI/CD
* Daria Miklashevskaya - Web Client
# Architectural Diagram
![](https://i.imgur.com/SgX8Quw.png)

## Example how some of the commands are executed 
### How new nodes enters the DFS
![](https://i.imgur.com/sIT7dG0.png)


### How client stores file
![](https://i.imgur.com/AVTsRH5.png)


# Communication protocol
We have used [GRPC](https://grpc.io/) for our communication protocol between DFS nodes themselves and between Client and DFS.

## Namenode
Namenode accepts following requests from Client:
* Init filesystem
* Get file/directory information
* Download/Store/Delete/Move/Copy Files
* Move/Delete/Create Directory

From Storage nodes Namenode accepts:
* Heartbeat 
* File acknowledge

## Storage Node
From Client Storage node accepts:
* Store/Get File

From Namenode Storage node accepts:
* Init filesystem
* Delete/Copy file

# Useful commands

## Update all submodules

    git submodule update --init --recursive

## Compile gRPC stubs for python

    python -m grpc_tools.protoc --proto_path=common/ --python_out=generated --grpc_python_out=generated ./common/routes.proto
    
## Compile gRPC stubs for javascript
    
    protoc --proto_path=common --js_out=import_style=commonjs,binary:generated/ --grpc-web_out=import_style=commonjs,mode=grpcwebtext:generated/ common/routes.proto

## Local Build Setup for Frontend

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
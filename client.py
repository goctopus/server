from __future__ import print_function

import logging
import os
import uuid

import grpc

from generated import routes_pb2_grpc, routes_pb2


def run():
    name_server_url = '18.197.113.217:9080'
    storage_server_url = '18.197.113.217:9080'
    with grpc.insecure_channel(storage_server_url) as storage_channel:
        storage_stub = routes_pb2_grpc.StorageServiceStub(storage_channel)
        with grpc.insecure_channel(name_server_url) as name_channel:

            name_stub = routes_pb2_grpc.NameServiceStub(name_channel)
            single_file = False
            if single_file:
                request = routes_pb2.StoreFileRequest(size=len(b'hello there'), file_path='/test_file.test')
                req = name_stub.StoreFile(request)
                print(req.file_uuid)

                request = routes_pb2.StorageFile(file_uuid=str(req.file_uuid), size=len(b'hello there'),
                                                 file=b'hello there', propagate=True)
                req = storage_stub.StoreFile(request)
                print(req)

            else:
                request = routes_pb2.InitRequest()
                size = name_stub.Init(request)
                print(size.available_size)

                # ask name node to store the file

                # Create file
                request = routes_pb2.StoreFileRequest(size=3, file_path='/name.test')
                req = name_stub.StoreFile(request)
                print(req.file_uuid)

                request = routes_pb2.StorageFile(file_uuid=str(req.file_uuid), size=3, file=b'123', propagate=True)
                req = storage_stub.StoreFile(request)
                print(req)

                request = routes_pb2.GetFileRequest(file_path='/name.test')
                req = name_stub.GetFile(request)
                print(req)

                request = routes_pb2.GetFileStorageRequest(file_uuid=req.file_uuid)
                req = storage_stub.GetFile(request)
                print(req)

                request = routes_pb2.StoreFileRequest(size=4, file_path='/lol.txt')
                req = name_stub.StoreFile(request)
                print(req.file_uuid)

                request = routes_pb2.StorageFile(file_uuid=str(req.file_uuid), size=4, file=b'1234', propagate=True)
                req = storage_stub.StoreFile(request)
                print(req)

                request = routes_pb2.DeleteFileRequest(file_path='/lol.txt')
                req = name_stub.DeleteFile(request)
                print(req)

                request = routes_pb2.MoveFileRequest(file_path='/name.test', new_path='/lol.test')
                req = name_stub.CopyFile(request)
                print(req)

                request = routes_pb2.CreateDirectoryRequest(path='/go')
                req = name_stub.StoreDirectory(request)
                print(req)

                request = routes_pb2.GetDirectoryInfoRequest(path='/')
                req = name_stub.GetDirectoryInfo(request)
                print(req)

                request = routes_pb2.MoveFileRequest(file_path='/lol.test', new_path='/go/lol.test')
                req = name_stub.MoveFile(request)
                print(req)

                request = routes_pb2.CreateDirectoryRequest(path='/go2')
                req = name_stub.StoreDirectory(request)
                print(req)

                request = routes_pb2.DeleteDirectoryRequest(path='/go2')
                req = name_stub.DeleteDirectory(request)
                print(req)


if __name__ == '__main__':
    logging.basicConfig()
    run()

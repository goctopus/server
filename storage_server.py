import json
import logging
import os
import urllib.request
from concurrent import futures
import grpc

from generated import routes_pb2_grpc, routes_pb2
# import route_guide_resources
from servers import StorageService
from utils import FileDoesntExist, FileAlreadyExists, DFSError


class RouteGuideServicer(routes_pb2_grpc.StorageServiceServicer):
    """Provides methods that implement functionality of route guide server."""

    def Init(self, request, context):
        path = f"{os.environ.get('STORAGE_SERVER_DATA_PATH')}"
        size = storage_service.init_filesystem(path)
        return routes_pb2.InitInfo(available_size=size)

    def GetFile(self, request, context):
        try:
            file = storage_service.read_file(request.file_uuid)
        except FileDoesntExist as e:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details(str(e))
            return routes_pb2.StorageFile()
        except DFSError as e:
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details(str(e))
            return routes_pb2.StorageFile()

        return routes_pb2.StorageFile(file_uuid=request.file_uuid, size=len(file), file=file)

    def StoreFile(self, request, context):
        try:
            storage_service.create_file(request.file_uuid, request.file, request.propagate)
        except DFSError as e:
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details(str(e))
            return routes_pb2.StorageFileInfo()
        return routes_pb2.StorageFileInfo(file_uuid=request.file_uuid)

    def RemoveFile(self, request, context):
        try:
            storage_service.delete_file(request.file_uuid)
        except FileDoesntExist as e:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details(str(e))
            return routes_pb2.StorageFileInfo()
        except DFSError as e:
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details(str(e))
            return routes_pb2.StorageFileInfo()
        return routes_pb2.StorageFileInfo(file_uuid=request.file_uuid)

    def CopyFile(self, request, context):
        try:
            storage_service.copy_file(request.source_file_uuid, request.target_file_uuid)
        except FileDoesntExist as e:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details(str(e))
            return routes_pb2.StorageFileInfo()
        except DFSError as e:
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details(str(e))
            return routes_pb2.StorageFileInfo()
        return routes_pb2.StorageFileInfo(file_uuid=request.target_file_uuid)


def serve(server_port):
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    routes_pb2_grpc.add_StorageServiceServicer_to_server(
        RouteGuideServicer(), server)
    server.add_insecure_port(f'[::]:{server_port}')
    server.start()
    server.wait_for_termination()


if __name__ == '__main__':
    logging.basicConfig()
    server_port = int(os.environ.get('STORAGE_SERVER_PORT'))
    print(f"Storage server started on port {server_port}")

    if os.environ.get('SENTRY_DSN'):
        import sentry_sdk
        sentry_sdk.init(os.environ.get('SENTRY_DSN'))

        storage_url = urllib.request.urlopen('http://169.254.169.254/latest/meta-data/local-ipv4').read().decode(
            'utf-8')
        storage_url += f':{server_port}'

        public_url = urllib.request.urlopen('http://169.254.169.254/latest/meta-data/public-ipv4').read().decode(
            'utf-8')
        public_url += f':{server_port}'

    else:
        storage_url = 'localhost'
        storage_url += f':{server_port}'
        public_url = storage_url

    storage_service = StorageService(url=storage_url)
    storage_service.heartbeat_daemon()
    serve(server_port)
